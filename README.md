# Kaleo Finance

**LIVE Demo:** [https://kaleo-finance-frontend-h7pi7igbcq-ew.a.run.app/](https://kaleo-finance-frontend-h7pi7igbcq-ew.a.run.app/)

**Demo Walkthough CodeLab**: [https://apigee-demos.web.app/codelabs/kaleo-finance/](https://apigee-demos.web.app/codelabs/kaleo-finance/)

This is an API ecosystem demo using APIs, AI services, and other 3rd party services to show how digital banking can leverage cloud platforms like Apigee and Google Cloud for their business platforms.

Here is the architecture of the demo:

![Kaleo Finance demo architecture](img/kaleo-finance-arch.png)

## Apps
This demo has 3 apps to demonstrate the use-case.

App | Screenshot | Description
--- | --- | ---
[First API Bank](https://emea-poc13-smartcredit.apigee.io/) | <img src="img/first-api-bank.png" width="400px"> | First API Bank homepage with the Financing API using Google ML
[Credit Chat](https://emea-poc13-smartcredit.apigee.io/chat) | <img src="img/first-api-chat.png" width="400px"> | Partner credit chat app using the same APIs as the homepage

## Deployment
Check out the [GitLab CI configuration](https://gitlab.com/tyayers/kaleo-finance/-/blob/master/.gitlab-ci.yml) of this project for an example deployment:
 * CI deployment of the Finance Proxy to Apigee
 * Container deployment of the web app frontend to Cloud Run
